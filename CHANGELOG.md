# Changelog

## 3.3.2 2025-01-25

### Security

- Bien tester l'autorisation d'afficher les listes de messages dans les fragments chargés en ajax

## 3.3.1 2025-01-16

### Security

- Bien tester les autorisations d'afficher le contenu des articles/rubriques dans les fragments chargés en ajax

### Fixed

- #4692 Ne pas générer d'erreur dans le json du calendrier quand le multilinguisme n'est pas encore configuré