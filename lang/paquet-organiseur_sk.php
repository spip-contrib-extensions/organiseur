<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'Nástroje na redakčnú prácu v skupine',
	'organiseur_slogan' => 'Nástroje na redakčnú prácu v skupine',
];
