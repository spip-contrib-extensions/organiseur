<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'Werkzeuge für eine Online-Redaktion',
	'organiseur_slogan' => 'Werkzeuge für eine Online-Redaktion',
];
