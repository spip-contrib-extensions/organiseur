<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=pt
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'Ferramentas de trabalho editorial em grupo',
	'organiseur_slogan' => 'Ferramentas de trabalho editorial em grupo',
];
