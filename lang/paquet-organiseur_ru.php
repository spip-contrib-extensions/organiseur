<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'Организация совместной работы',
	'organiseur_slogan' => 'Организация совместной работы',
];
