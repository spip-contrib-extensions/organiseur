<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'Collaborative editorial working tools',
	'organiseur_slogan' => 'Collaborative editorial working tools',
];
