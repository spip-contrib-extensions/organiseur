<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=fr_fem
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'Outils de travail éditorial en groupe',
	'organiseur_slogan' => 'Outils de travail éditorial en groupe',
];
