<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'ابزارهاي گروه سردبيري',
	'organiseur_slogan' => 'ابزارهاي كار گروه سردبيري ',
];
