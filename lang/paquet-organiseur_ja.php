<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=ja
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'グループ編集作業ツール',
	'organiseur_slogan' => 'グループ編集作業ツール',
];
