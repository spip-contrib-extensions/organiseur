<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-organiseur?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// O
	'organiseur_description' => 'Herramientas de trabajo editorial en grupo',
	'organiseur_slogan' => 'Herramientas de trabajo editorial en grupo',
];
